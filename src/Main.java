

import java.util.Scanner;
import java.util.Random;
import sorting.BubbleSort;
import sorting.InsertionSort;
import sorting.SelectionSort;
    public class Main {
        public static void main(String[] args) {
            char[] array = new char[] {'1','8','5','7','a','T','f'};
            SelectionSort.sort(array);
            BubbleSort.sort(array);
            InsertionSort.sort(array);
            for (char ch: array){
                System.out.println(ch);
            }
        }
    }