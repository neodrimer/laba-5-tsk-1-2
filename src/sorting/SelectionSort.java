package sorting;
public class SelectionSort {
    public static void sort(char arr[]) {
        int pos;
        char temp;
        for (int i = 0; i < arr.length; i++) {
            pos = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[pos]) {
                    pos = j;
                }
            }
            temp = arr[pos];
            arr[pos] = arr[i];
            arr[i] = temp;
        }
    }
    public static void sort(String[] array) {
        for ( int j=0; j < array.length-1; j++ )
        {
            int min = j;
            for ( int k=j+1; k < array.length; k++ )
                if ( array[k].compareTo( array[min] ) < 0 ) min = k;
            String temp = array[j];
            array[j] = array[min];
            array[min] = temp;
        }
    }


}
