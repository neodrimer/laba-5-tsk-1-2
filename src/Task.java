

import java.util.Scanner;
import java.util.Random;
import sorting.BubbleSort;
import sorting.InsertionSort;
import sorting.SelectionSort;
public class Task {
        public static void main(String[] args) {
            String[] array = {"Dzmitry","Andrey"};
            SelectionSort.sort(array);
            BubbleSort.sort(array);
            InsertionSort.sort(array);
            for (String str: array){
                System.out.println(str);
            }
        }
    }

